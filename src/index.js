import React from 'react';
import ReactDOM from 'react-dom';
import './lib/bootstrap/css/bootstrap.css';
import './lib/font-awesome/css/font-awesome.css';


// import './index.css';
import App from './App';
// import './lib/jquery/jquery.min.js';
// import './lib/bootstrap/js/bootstrap.js';
import * as serviceWorker from './serviceWorker';


ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
