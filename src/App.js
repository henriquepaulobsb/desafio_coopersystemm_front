import React, { Component } from 'react';
import { BrowserRouter, Route } from 'react-router-dom'
import Header from './component/Header'
import SideBar from './component/SideBar'
import Footer from './component/Footer'
import FormCompenent from './view/FormCompenent'
import Lista from './view/Lista'
import EditForm from './view/EditForm'
import Detalhar from './view/Detalhar'

import "./App.css";
import "./index.css";
import "./estilo.css";
import "./lib/bootstrap/css/style-responsive.css";
// import "./lib/js/common-scripts.js";


class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <div >
          <Header />
          <SideBar />
          <Route path='/cadastro' exact  component={FormCompenent}/>
          <Route path='/update/:id'  component={EditForm}/>
          <Route path='/detalhar/:id'  component={Detalhar}/>
          <Route path='/lista'  component={Lista}/>
          <Footer />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
