import React from 'react'
import { Link } from 'react-router-dom'
import imgUser from '../img/ui-sam.jpg'
import {Sidebar} from 'react-bootstrap'
const SideBar = props => {
    return (
        <div>
            <aside>
                <div id='sidebar' className='nav-collapse '>
                    {/* sidebar menu start*/}
                    <ul className='sidebar-menu' id='nav-accordion'>
                        <p className='centered'><a href='profile.html'>
                            <img src={imgUser} className='img-circle' width='80' />
                        </a>
                        </p>
                        <li>
                            <Link to='/cadastro'>
                                <i className='fa fa-cogs'></i>
                                <span>Cadastro Cliente</span>
                            </Link>
                        </li>
                        <li>
                            <Link to='/lista'>
                                <i className='fa fa-th'></i>
                                <span>Lista Clientes</span>
                            </Link>
                        </li>
                    </ul>
                    <br/>
                </div>

            </aside>
           
        </div>
    )

}
export default SideBar;