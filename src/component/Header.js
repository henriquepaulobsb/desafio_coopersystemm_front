import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Nav, NavItem, Dropdown, DropdownItem, DropdownToggle, DropdownMenu, NavLink } from 'reactstrap';

class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
        };
    }


    render() {
        return (
            <div>

                {/* <nav class="nav navbar-fixed-top" role="navigation">
                    <div class="container">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Navegue pelo site</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="/">Mapa da Previdência</a>
                        </div>
                        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li class="dropdown visible-xs-block">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-location-arrow"></i> Estados <span class="caret"></span></a>
                                </li>
                                <li class="hidden-sm">
                                    <a href="/partidos/"><i class="fa fa-flag"></i> Partidos</a>
                                </li>

                                <li>
                                    <a href="/criterios.html"><i class="fa fa-gears"></i> Critérios</a>
                                </li>

                                <li>
                                    <a href="/achou-um-erro.html"><i class="fa fa-edit"></i> Achou um erro?</a>
                                </li>

                                <li>
                                    <a href="mailto:mapa@vemprarua.net"><i class="fa fa-envelope"></i> Contato</a>
                                </li>
                            </ul>
                            <div class="col-sm-3 col-md-3 pull-right">
                                <form class="navbar-form" role="search" id="busca" onsubmit="return false;">
                                    <div class="input-group">
                                        <input id="q" type="text" class="form-control" placeholder="Busca" name="q" autocomplete="off" spellcheck="false" />
                                        <div class="input-group-btn">
                                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </nav> */}


                <header className='header black-bg'>
                 <div className='sidebar-toggle-box'>
                        <div className='fa fa-bars tooltips' data-placement='right' data-original-title='Toggle Navigation'></div>
                    </div>
                    <Link to='/' className='logo'><b>Desafio<span>CooperSystem</span></b></Link>
                    <div className='top-menu'>
                        <ul className='nav pull-right top-menu'>
                            <li><a className='logout' href='login.html'>Logout</a></li>
                        </ul>
                    </div>
                </header>
            </div>
        )
    }
}

export default Header;
