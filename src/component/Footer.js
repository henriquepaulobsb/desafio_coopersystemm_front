import React from 'react'

const Footer = props => {
    return (
        <div>
            <footer className='site-footer'>
                <div className='text-center'>
                    <p>
                        &copy; Desefio <strong>CooperSystem</strong>. Todos os direitos reservados
                    </p>
                </div>
            </footer>
        </div>
    )
}
export default Footer;