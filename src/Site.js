import React from 'react'

const Site = props => {
    return (
        <div>
            <body>
                <section id='container'>
                    <section id='main-content'>
                        <section className='wrapper'>
                            <div className='row'>
                            <div className='col-md-4 col-sm-4 mb'>
                            
                            </div>

                                <div className='col-lg-12 main-chart'>
                                    {/*CUSTOM CHART START */}
                                    <div className='border-head'>
                                        <h3>USER VISITS</h3>
                                    </div>
                                    <div className='custom-bar-chart'>
                                        <ul className='y-axis'>
                                            <li><span>10.000</span></li>
                                            <li><span>8.000</span></li>
                                            <li><span>6.000</span></li>
                                            <li><span>4.000</span></li>
                                            <li><span>2.000</span></li>
                                            <li><span>0</span></li>
                                        </ul>
                                        <div className='bar'>
                                            <div className='title'>JAN</div>
                                            <div className='value tooltips' data-original-title='8.500' data-toggle='tooltip' data-placement='top'>85%</div>
                                        </div>
                                        <div className='bar '>
                                            <div className='title'>FEB</div>
                                            <div className='value tooltips' data-original-title='5.000' data-toggle='tooltip' data-placement='top'>50%</div>
                                        </div>
                                        <div className='bar '>
                                            <div className='title'>MAR</div>
                                            <div className='value tooltips' data-original-title='6.000' data-toggle='tooltip' data-placement='top'>60%</div>
                                        </div>
                                        <div className='bar '>
                                            <div className='title'>APR</div>
                                            <div className='value tooltips' data-original-title='4.500' data-toggle='tooltip' data-placement='top'>45%</div>
                                        </div>
                                        <div className='bar'>
                                            <div className='title'>MAY</div>
                                            <div className='value tooltips' data-original-title='3.200' data-toggle='tooltip' data-placement='top'>32%</div>
                                        </div>
                                        <div className='bar '>
                                            <div className='title'>JUN</div>
                                            <div className='value tooltips' data-original-title='6.200' data-toggle='tooltip' data-placement='top'>62%</div>
                                        </div>
                                        <div className='bar'>
                                            <div className='title'>JUL</div>
                                            <div className='value tooltips' data-original-title='7.500' data-toggle='tooltip' data-placement='top'>75%</div>
                                        </div>
                                    </div>
                                    {/*custom chart end*/}
                                    <div className='row mt'>
                                        {/* SERVER STATUS PANELS */}
                                        <div className='col-md-4 col-sm-4 mb'>
                                            <div className='grey-panel pn donut-chart'>
                                                <div className='grey-header'>
                                                    <h5>SERVER LOAD</h5>
                                                </div>
                                                <canvas id='serverstatus01' height='120' width='120'></canvas>
                                                
                                                <div className='row'>
                                                    <div className='col-sm-6 col-xs-6 goleft'>
                                                        <p>Usage<br />Increase:</p>
                                                    </div>
                                                    <div className='col-sm-6 col-xs-6'>
                                                        <h2>21%</h2>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                        <div className='col-md-4 col-sm-4 mb'>
                                            <div className='darkblue-panel pn'>
                                                <div className='darkblue-header'>
                                                    <h5>DROPBOX STATICS</h5>
                                                </div>
                                                <canvas id='serverstatus02' height='120' width='120'></canvas>
                                                <p>April 17, 2014</p>
                                                <footer>
                                                    <div className='pull-left'>
                                                        <h5><i className='fa fa-hdd-o'></i> 17 GB</h5>
                                                    </div>
                                                    <div className='pull-right'>
                                                        <h5>60% Used</h5>
                                                    </div>
                                                </footer>
                                            </div>
                                        </div>
                                        <div className='col-md-4 col-sm-4 mb'>
                                            <div className='green-panel pn'>
                                                <div className='green-header'>
                                                    <h5>REVENUE</h5>
                                                </div>
                                                <div className='chart mt'>
                                                    <div className='sparkline' data-type='line' data-resize='true' data-height='75' data-width='90%' data-line-width='1' data-line-color='#fff' data-spot-color='#fff' data-fill-color='' data-highlight-line-color='#fff' data-spot-radius='4' data-data='[200,135,667,333,526,996,564,123,890,464,655]'></div>
                                                </div>
                                                <p className='mt'><b>$ 17,980</b><br />Month Income</p>
                                            </div>
                                        </div>
                                        {/* /col-md-4 */}
                                    </div>

                                    <div className='row'>
                                    </div>
                                    <div className='row'>
                                        <div className='col-lg-4 col-md-4 col-sm-4 mb'>
                                            <div className='product-panel-2 pn'>
                                                <div className='badge badge-hot'>HOT</div>
                                                <img src='img/product.jpg' width='200' alt='' />
                                                <h5 className='mt'>Flat Pack Heritage</h5>
                                                <h6>TOTAL SALES: 1388</h6>
                                                <button className='btn btn-small btn-theme04'>FULL REPORT</button>
                                            </div>
                                        </div>
                                      
                                        <div className='col-lg-4 col-md-4 col-sm-4 mb'>
                                            <div className='content-panel pn'>
                                                <div id='profile-02'>
                                                    <div className='user'>
                                                        <img src='img/friends/fr-06.jpg' className='img-circle' width='80' />
                                                        <h4>DJ SHERMAN</h4>
                                                    </div>
                                                </div>
                                                <div className='pr2-social centered'>
                                                    <a href='#'><i className='fa fa-twitter'></i></a>
                                                    <a href='#'><i className='fa fa-facebook'></i></a>
                                                    <a href='#'><i className='fa fa-dribbble'></i></a>
                                                </div>
                                            </div>

                                        </div>
                                        <div className='col-md-4 col-sm-4 mb'>
                                            <div className='green-panel pn'>
                                                <div className='green-header'>
                                                    <h5>DISK SPACE</h5>
                                                </div>
                                                <canvas id='serverstatus03' height='120' width='120'></canvas>
                                                <h3>60% USED</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </section>
                   
                    
                </section>
            </body>

        </div>
    );
}
export default Site;