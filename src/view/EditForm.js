import React, { Component } from 'react'
import axios from 'axios'
import InputMask from 'react-input-mask';
import ViaCep from 'react-via-cep';

const emailRegex = RegExp(
  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);

class EditForm extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: '',
      name: '',
      telefone: '',
      cpf: '',
      date: new Date(),
      indexEmail: '',
      endereco: {
        id: '',
        cep: '',
        logradouro: '',
        complemento: '',
        bairro: '',
        localidade: '',
        uf: '',
      },
      emailObject: {
        email: '',
        id: '',
        principal: null
      },
      formErrors: {
        name: '',
        email: '',
        cpf: '',
        telefone: '',
        cep: '',
      },
      fields: {},
      enderecos: [],
      emails: [],
      errors: {},
      posicaoEncotradada: -1,
      posicaoEncotradadaEnd: -1,
      messagem:''
    };

    this.handleChangeCep = this.handleChangeCep.bind(this);
    this.handleSuccess = this.handleSuccess.bind(this);
    this.cancelConsultaCep = this.cancelConsultaCep.bind(this);
    this.handleChangeEndereco = this.handleChangeEndereco.bind(this);
    this.handleAddEndereco = this.handleAddEndereco.bind(this);
    this.handleDelItem = this.handleDelItem.bind(this)
    this.handleAddEmail = this.handleAddEmail.bind(this)
    this.handleDelEmail = this.handleDelEmail.bind(this)
    this.handleEditarEmail = this.handleEditarEmail.bind(this)
  }

  componentWillMount() {
    this.getMeetupDetails();
  }


  getMeetupDetails() {
    const meetupId = this.props.match.params.id;
    if (meetupId !== undefined) {
      axios.get(`http://localhost:8080/api/cliente/` + meetupId)
        .then(response => {
          this.setState({
            id: response.data.id,
            name: response.data.name,
            cpf: response.data.cpf,
            telefone: response.data.telefone,
            enderecos: response.data.enderecos,
            emails: response.data.emails,
          }, () => {
          });
        })
        .catch(err => console.log(err));
    }
  }

  handleEdit = e => {
    e.preventDefault();
    if (this.validateForm()) {
      axios.put(`http://localhost:8080/api/update`, {
        id: this.state.id,
        name: this.state.name,
        cpf: this.state.cpf,
        telefone: this.state.telefone,
        enderecos: this.state.enderecos,
        emails: this.state.emails,
      })
        .then(res => {
          this.setState({ messagem: res.data });
        })
    } else {
      console.error("FORM INVALID - DISPLAY ERROR MESSAGE");
    }
  };

  validateForm() {
    
    let formIsValid = true;
    let fields = this.state;
    let errors = {};

    if (fields.name === "") {
      formIsValid = false;
      errors["name"] = "*Favor informe nome.";
    }
    
    if (this.state.emails.length === 0) {
      formIsValid = false;
      errors["email"] = "*Informe pelo menos um Email.";
    } else if (typeof fields.email !== "undefined") {
      var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      if (!pattern.test(fields.email)) {
        formIsValid = false;
        errors["email"] = "*Email Inválido.";
      }
    }

    if(this.state.enderecos.length === 0){
      formIsValid = false;
      errors['cep'] = '*CEP precisa ser Adicionado.';
   }

    this.setState({
      errors: errors
    });
    return formIsValid;
  }

  handleChange = e => {
    e.preventDefault();
    const { name, value } = e.target;
    let formErrors = { ...this.state.formErrors };

    switch (name) {
      case "name":
        formErrors.name = value.length < 3 ? "Mínimo de 3 caracteres necessários" : "";
        break;
      case "email":
       this.state.emailObject.email = value;
        formErrors.email = emailRegex.test(value) ? "" : "Endereço de email invalido";
        break;
      default:
        break;
    }
    this.setState({ formErrors, [name]: value }, () => console.log(this.state));
  };

  handleChangeCep(evt) {
    this.setState({ cep: evt.target.value })
  }

  handleSuccess(data) {
    this.setState({
      endereco: {
        cep: data.cep,
        logradouro: data.logradouro,
        complemento: data.complemento,
        bairro: data.bairro,
        localidade: data.localidade,
        uf: data.uf
      }
    })
  }

  handleChangeEndereco = e => {
    e.preventDefault();
    const { name, value } = e.target;
    let formErrors = { ...this.state.formErrors };

    switch (name) {
      case 'bairro':
        this.state.endereco.bairro = value;
        break;
      case 'logradouro':
        this.state.endereco.logradouro = value;
        break;
      case 'complemento':
        this.state.endereco.complemento = value;
        break;
      case 'complemento':
        this.state.endereco.complemento = value;
        break;
      default:
        break;
    }
    this.setState({ formErrors, [name]: value }, () => console.log(this.state));
  };

  cancelConsultaCep() {
    this.state.posicaoEncotradadaEnd = -1;
    this.setState({
      cep: '',
      endereco: {
        cep: '',
        logradouro: '',
        complemento: '',
        bairro: '',
        localidade: '',
        uf: '',
      },
    })
  }

  handleAddEmail() {
    let errors = {};
    if(this.state.emailObject.email !== ''){
      let number_of_elements_to_remove = 1;
      if(this.state.posicaoEncotradada === -1){
         this.state.emails.push(this.state.emailObject)
      }else{
        this.state.emails.splice(this.state.posicaoEncotradada, number_of_elements_to_remove,this.state.emailObject);
        this.state.posicaoEncotradada = -1
      }
    }else{
       errors["email"] = "*Favor informe seu Email.";
    }
    this.setState({
      errors: errors,
      emailObject: {
        email: ''
      }
     }
    )
  }

  handleEditarEmail(dado, index) {
    this.state.posicaoEncotradada = index
    this.setState({
      emailObject: {
        id: dado.id,
        email: dado.email,
        principal: dado.principal
      },
    }
    )
  }

  handleAddEndereco() {
    this.state.endereco.complemento = this.state.complemento
    let number_of_elements_to_remove = 1;
    if(this.state.posicaoEncotradadaEnd === -1){
       this.state.enderecos.push(this.state.endereco)
    }else{
      this.state.enderecos.splice(this.state.posicaoEncotradadaEnd, number_of_elements_to_remove,this.state.endereco);
      this.state.posicaoEncotradadaEnd = -1
    }
    
    this.setState({
      endereco: {
        cep: '',
        logradouro: '',
        complemento: '',
        bairro: '',
        localidade: '',
        uf: '',
      },
      cep: '',
      complemento: ''
    }
    )
  }

  handleEditarEndereco(dado,index) {
    console.log("Index: "+index);
    this.state.posicaoEncotradadaEnd = index
    this.setState({
      endereco: {
        id: dado.id,
        cep: dado.cep,
        logradouro: dado.logradouro,
        complemento: dado.complemento,
        bairro: dado.bairro,
        localidade: dado.localidade,
        uf: dado.uf,
      },
      cep: dado.cep
    }
    )
  }
 

   insertItem(array, action) {
    let newArray = array.slice()
    newArray.splice(action.index, 0, action.item)
    return newArray
  }

  handleDelItem = v => {
    for (var i = 0; i < this.state.enderecos.length; i++) {
      if (this.state.enderecos[i] === v) {
        delete this.state.enderecos[i]
        this.state.enderecos.splice(i,1);
      }
    }
    this.setState({
      enderecos: this.state.enderecos
    })
  }

  handleDelEmail = v => {
    for (var i = 0; i < this.state.emails.length; i++) {
      if (this.state.emails[i] === v) {
        delete this.state.emails[i]
        this.state.emails.splice(i,1);
      }
    }
    this.setState({
      emails: this.state.emails
    })
  }

  handleFormReset = () => {
    this.state.email = ""
    this.setState(() => this.initialState)
  }

  cancel() {
    console.log('Emails: '+this.state.emails.length)
    this.setState({
      name: "",
      email: "",
    })
  }

  render() {
    const { formErrors } = this.state;
    const { msg } = this.state.messagem;
    const name = this.state;

    return (
      <div>
        <section id='main-content'>
          <section className='wrapper'>
            <h3><i className='fa fa-angle-right'></i> Editar </h3>
            <div className='row mt'>

              <div className='col-lg-12'>
                <div className='form-panel'>
                  <h4 className='mb'><i className='fa fa-angle-right'></i>Editar</h4>
                  {
                    this.state.messagem !== "" &&
                    <div className='alert alert-success'>
                      {this.state.messagem}
                    </div>
                  }
                  <div className='form-horizontal style-form' >
                    <div className='form-group'>
                      <label className='col-sm-2 col-sm-2 control-label'>Nome</label>
                      <div className='col-sm-10'>

                        <input type='text'
                          name='name'
                          className={formErrors.name.length > 0 ? "form-control error" : "form-control"}
                          placeholder='Nome'
                          value={this.state.name}
                          onChange={this.handleChange}
                        />
                        {formErrors.name.length > 0 && (
                          <span className="form-group error">{formErrors.name}</span>
                        )}
                        {this.state.errors.name !== "" && (
                          <div className="error">{this.state.errors.name}</div>
                        )}
                      </div>
                    </div>

                    <div className='form-group'>
                      <label className='col-sm-2 col-sm-2 control-label'>CPF</label>
                      <div className='col-sm-10'>

                        <InputMask mask='999.999.999-99'
                          name='cpf'
                          value={this.state.cpf}
                          onChange={this.handleChange}
                          placeholder='cpf'
                          className={formErrors.cpf.length > 0 ? 'form-control error' : 'form-control'}
                        />

                        {formErrors.cpf.length > 0 && (
                          <span className='form-group error '>{formErrors.cpf}</span>
                        )}
                        {this.state.errors.cpf !== '' && (
                          <div className='error'>{this.state.errors.cpf}</div>
                        )}
                      </div>
                    </div>

                    <div className='form-group'>
                      <label className='col-sm-2 col-sm-2 control-label'>Telefone</label>
                      <div className='col-sm-10'>
                        <InputMask mask='(99) 99999 99-99'
                          name='telefone'
                          value={this.state.telefone}
                          onChange={this.handleChange}
                          placeholder='Telefone'
                          className={formErrors.telefone.length > 0 ? 'form-control error' : 'form-control'}
                        />
                        {formErrors.name.length > 0 && (
                          <span className='form-group error'>{formErrors.telefone}</span>
                        )}
                        {this.state.errors.name !== '' && (
                          <div className='error'>{this.state.errors.telefone}</div>
                        )}
                      </div>
                    </div>

                    <div className='form-group'>
                      <label className='col-lg-2 col-sm-2 control-label'>Email</label>
                      <div className='col-lg-8 col-sm-8'>
                        <input
                          className={formErrors.email.length > 0 ? 'form-control error' : 'form-control'}
                          placeholder='Email'
                          type='email'
                          name='email'
                          noValidate
                          value={this.state.emailObject.email}
                          onChange={this.handleChange}
                        />
                        {formErrors.email.length > 0 && (
                          <span className='error '>{formErrors.email}</span>
                        )}
                        {this.state.errors.email !== '' && (
                          <div className='error'>{this.state.errors.email}</div>
                        )}
                      </div>
                      <div className='col-lg-2 col-sm-2'>
                        <button className='btn btn-theme' onClick={this.handleAddEmail}>Add Email</button>
                      </div>

                      {this.state.emails.map((data, index) => {
                        return <div>
                          <div  className='col-lg-3 col-sm-3'>
                            <li key={index}>
                              <p>
                                Email: {data.email} <br />
                              </p>
                              <button className='btn btn-danger btn-xs' style={{ margin: '3px' }}
                                onClick={this.handleDelEmail.bind(this, data)}>Remover</button>
                              <button className='btn btn-warning btn-xs'
                                onClick={this.handleEditarEmail.bind(this, data, index)}>Ediar</button>
                            </li>
                          </div>
                        </div>

                      })}
                      <br />
                    </div>

                    <ViaCep cep={this.state.cep} onSuccess={this.handleSuccess} lazy>
                      {({ data, loading, error, fetch }) => {
                        if (loading) {
                          return <p>loading...</p>
                        }
                        return <div className='form-group'>
                          <label className='col-lg-2 col-sm-2 control-label'>CEP</label>
                          <div className='col-lg-8 col-sm-8'>
                            <InputMask mask='99999-999'
                              className={'form-control'} name='cep' value={this.state.cep}
                              placeholder='CEP' onChange={this.handleChangeCep}
                            />
                            {formErrors.cep.length > 0 && (
                              <span className='error'>{formErrors.cep}</span>
                            )}
                            {this.state.errors.cep !== '' && (
                              <div className='error'>{this.state.errors.cep}</div>
                            )}
                          </div>
                          <div className='col-lg-2 col-sm-2'>
                            <button className='btn btn-theme' onClick={fetch}>Pesquisar</button>
                          </div>

                        </div>
                      }}
                    </ViaCep>

                    {this.state.endereco.cep !== '' && this.state.endereco.cep !== undefined && (
                      <div className='form-group'>
                        <label className='col-lg-2 col-sm-2 control-label'>Endereço</label>
                        <div className='col-lg-8 col-sm-8'>
                          <div className='col-md-4'>
                            <label  className='control-label'>CEP</label>
                            <input type='text' disabled name='cep' value={this.state.endereco.cep}
                              className='form-control' />
                          </div>

                          <div className='col-lg-4 col-sm-4 col-md-4'>
                            <label for='idCidade' className='control-label'>Cidade</label>
                            <input type='text' name='endereco.localidade' disabled
                              value={this.state.endereco.localidade}
                              className='form-control' id='idCidade' />
                          </div>

                          <div className='col-md-4'>
                            <label for='idBairro' className='control-label'>Bairro</label>
                            <input type='text' name='bairro' value={this.state.endereco.bairro}
                              onChange={this.handleChangeEndereco}
                              className='form-control' id='idBairro' />
                          </div>

                          <div className='col-md-4'>
                            <label for='idLogradouro' className='control-label'>Logradouro</label>
                            <input type='text' name='logradouro' value={this.state.endereco.logradouro}
                              onChange={this.handleChangeEndereco}
                              className='form-control' id='idLogradouro' />

                          </div>

                          <div className='col-lg-4 col-sm-4 col-md-2'>
                            <label for='idUf' class='control-label'>UF</label>
                            <input type='text' name='uf' disabled value={this.state.endereco.uf}
                              className='form-control' id='idUf' />
                          </div>

                          <div className='col-lg-4 col-sm-4 col-md-4'>
                            <label for='idComplemento' class='control-label'>Complemento</label>
                            <input type='text' name='complemento' value={this.state.complemento}
                              className='form-control' onChange={this.handleChangeEndereco} />

                          </div>
                        </div>

                        <div className='col-lg-2'>
                          <button className='btn btn-theme' style={{ margin: '3px' }} onClick={this.cancelConsultaCep}>Cancelar</button>
                          <button className='btn btn-theme' onClick={this.handleAddEndereco}> 
                          { console.log("posicaoEncotradadaEnd: "+ this.state.posicaoEncotradadaEnd)   } 
                            { this.state.posicaoEncotradadaEnd === -1 ? 'Add' : 'Alterar' } 
                          </button>
                        </div>
                      </div>
                    )}
                    {this.state.enderecos.map((data, index) => {
                      return <div>
                        <div className='col-lg-3'>
                          <li key={index}>
                            <p>
                              CEP: {data.cep} <br />
                              CIDADE: {data.localidade} <br />
                              Bairro: {data.bairro} <br />
                              Logradouro: {data.logradouro} <br />
                              Complemento: {data.complemento} <br />
                              UF: {data.uf} <br />
                            </p>
                            <button className='btn btn-danger btn-xs' style={{ margin: '3px' }}
                              onClick={this.handleDelItem.bind(this, data)}>Remover</button>
                            <button className='btn btn-warning btn-xs'
                              onClick={this.handleEditarEndereco.bind(this, data, index)}>Ediar</button>
                          </li>
                        </div>
                      </div>
                    })}
                    <br />
                    <div className='form-group'>
                      <div className='col-lg-offset-2 col-lg-10'>
                        <button className='btn btn-theme' onClick={this.handleEdit}>Alterar</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br />
            <br />
          </section>
        </section >
      </div >
    )
  }
}
export default EditForm;