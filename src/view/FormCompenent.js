import React, { Component } from 'react'
import axios from 'axios'
import InputMask from 'react-input-mask';
import ViaCep from 'react-via-cep';

const emailRegex = RegExp(
  /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
);


class FormCompenent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      id: '',
      name: '',
      cpf: '',
      telefone: '',
      cep: '',
      endereco: {
        cep: '',
        logradouro: '',
        complemento: '',
        bairro: '',
        localidade: '',
         uf: '',
      },
      emailObject:{
        email: ''
      },
      formErrors: {
        name: '',
        email: '',
        cpf: '',
        telefone: '',
        cep: '',
      },
      fields: {},
      enderecos: [],
      emails: [],
      errors: {},
      messagem: ''
    };
    this.handleChangeCep = this.handleChangeCep.bind(this);
    this.handleSuccess = this.handleSuccess.bind(this);
    this.cancelConsultaCep = this.cancelConsultaCep.bind(this);
    this.handleChangeEndereco = this.handleChangeEndereco.bind(this);
    this.handleAddEndereco = this.handleAddEndereco.bind(this);
    this.handleDelItem = this.handleDelItem.bind(this)
    this.handleAddEmail = this.handleAddEmail.bind(this)
    this.handleDelEmail = this.handleDelEmail.bind(this)
    
  }

  handleChangeCep(evt) {
    this.setState({ cep: evt.target.value })
  }

  handleSuccess(data) {
    this.setState({
      endereco: {
        cep: data.cep,
        logradouro: data.logradouro,
        complemento: data.complemento,
        bairro: data.bairro,
        localidade: data.localidade,
        uf: data.uf
      }
    })
  }

  handleSubmit = e => {
    e.preventDefault();
    if (this.validateForm()) {
      axios.post(`http://localhost:8080/api/cliente`, {
        name: this.state.name,
        cpf: this.state.cpf.replace(/\D+/g, ''),
        telefone: this.state.telefone.replace(/\D+/g, ''),
        enderecos: this.state.enderecos,
        emails: this.state.emails,
      })
        .then(res => {
          this.setState({ messagem: res.data });
          this.cancel();
        })
    } else {
      console.error('FORM INVALID - DISPLAY ERROR MESSAGE');
    }
  };

  validateForm() {
    let formIsValid = true;
    let fields = this.state;
    let errors = {};

    if (fields.name === '') {
      formIsValid = false;
      errors['name'] = '*Favor informe nome.';
    }

    if (this.state.emails.length === 0) {
      formIsValid = false;
      errors["email"] = "*Informe pelo menos um Email.";
    } else
     if (typeof fields.email !== 'undefined') {
      var pattern = new RegExp(/^(('[\w-\s]+')|([\w-]+(?:\.[\w-]+)*)|('[\w-\s]+')([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
      if (!pattern.test(fields.email)) {
        formIsValid = false;
        errors['email'] = '*Email Inválido.';
      }
    }

    if (fields.cpf === '') {
      formIsValid = false;
      errors['cpf'] = '*Favor informar seu cpf.';
    }

    if (fields.telefone === '') {
      formIsValid = false;
      errors['telefone'] = '*Favor informar seu telefone.';
    }

    // if (fields.cep === '') {
    //   formIsValid = false;
    //   errors['cep'] = '*Favor informar seu CEP.';
    // }

    if(this.state.enderecos.length === 0){
       formIsValid = false;
       errors['cep'] = '*CEP precisa ser Adicionado.';
    }
    
    this.setState({
      errors: errors
    });
    return formIsValid;
  }

  handleChange = e => {
    e.preventDefault();
    const { name, value } = e.target;
    let formErrors = { ...this.state.formErrors };

    switch (name) {
      case 'name':
        formErrors.name = value.length < 3 ? 'Mínimo de 3 caracteres necessários' : '';
        break;
      case 'email':
        this.state.emailObject.email = value;
        formErrors.email = emailRegex.test(value) ? '' : 'Endereço de email invalido';
        break;
      case 'cpf':
        formErrors.cpf = value.length < 6 ? 'Mínimo de 6 caracteres necessários' : '';
        break;
      default:
        break;
    }
    this.setState({ formErrors, [name]: value }, () => console.log(this.state));
  };

  handleChangeEndereco = e => {
    e.preventDefault();
    const { name, value } = e.target;
    let formErrors = { ...this.state.formErrors };

    switch (name) {
      case 'bairro':
        this.state.endereco.bairro = value;
        break;
        case 'logradouro':
        this.state.endereco.logradouro = value;
        break;
        case 'complemento':
        this.state.endereco.complemento = value;
        break;
        case 'complemento':
        this.state.endereco.complemento = value;
        break;
      default:
        break;
    }
    this.setState({ formErrors, [name]: value }, () => console.log(this.state));
  };

  cancel() {
    this.setState({
      name: '',
      cpf: '',
      telefone: '',
      complemento: '',
      enderecos: [],
      emails:[],
      cep: '',
    })
  }

  cancelConsultaCep() {
    this.setState({
      cep: '',
      endereco: {
        cep: '',
        logradouro: '',
        complemento: '',
        bairro: '',
        localidade: '',
         uf: '',
       },
    })
  }

  handleAddEmail() {
    console.log("Email: "+this.state.emailObject)
    let errors = {};
    if(this.state.emailObject.email !== ''){
      this.state.emails.push(this.state.emailObject)
    }else{
      errors["email"] = "*Favor informe seu Email.";
    }

    this.setState({
      errors: errors,
      emailObject: {
        email: ''
      }
     }
    )

    console.log("Lista Email: "+this.state.emails)
  }

  handleAddEndereco() {
    this.state.endereco.complemento = this.state.complemento
    this.state.enderecos.push(this.state.endereco)
    this.setState({
      endereco: {
        cep: '',
        logradouro: '',
        complemento: '',
        bairro: '',
        localidade: '',
         uf: '',
       },
       cep: '',
       complemento: ''
      }
    )
  }

  handleDelItem = v => {
    for(var i = 0; i < this.state.enderecos.length; i++){
      if(this.state.enderecos[i] === v){
         delete this.state.enderecos[i]
      } 
    }
    this.setState({
      enderecos:this.state.enderecos
    })
  }

  handleDelEmail = v => {
    for(var i = 0; i < this.state.emails.length; i++){
      if(this.state.emails[i] === v){
         delete this.state.emails[i]
      } 
    }
    this.setState({
      emails:this.state.emails
    })
  }
  render() {
    const { formErrors } = this.state;
    const { msg } = this.state.messagem;
    const name = this.state;
    return (
      <div>
        <section id='main-content'>
          <section className='wrapper'>
            <h3><i className='fa fa-angle-right'></i> Registrar Cliente</h3>
            <div className='row mt'>
              <div className='col-lg-12'>
                <div className='form-panel'>
                  {
                    this.state.messagem !== '' &&
                    <div className='alert alert-success'>
                      {this.state.messagem}
                    </div>
                  }
                  <div className='form-horizontal style-form' >
                    <div className='form-group'>
                      <label className='col-sm-2 col-sm-2 control-label'>Nome</label>
                      <div className='col-sm-10'>
                        <input type='text'
                          name='name'
                          className={formErrors.name.length > 0 ? 'form-control error' : 'form-control'}
                          placeholder='Nome'
                          value={this.state.name}
                          onChange={this.handleChange}
                        />
                        {formErrors.name.length > 0 && (
                          <span className='error'>{formErrors.name}</span>
                        )}
                        {this.state.errors.name !== '' && (
                          <div className='error'>{this.state.errors.name}</div>
                        )}
                      </div>
                    </div>

                    <div className='form-group'>
                      <label className='col-sm-2 col-sm-2 control-label'>CPF</label>
                      <div className='col-sm-10'>

                        <InputMask mask='999.999.999-99'
                          name='cpf'
                          value={this.state.cpf}
                          onChange={this.handleChange}
                          placeholder='cpf'
                          className={formErrors.cpf.length > 0 ? 'form-control error' : 'form-control'}
                        />

                        {formErrors.cpf.length > 0 && (
                          <span className='error '>{formErrors.cpf}</span>
                        )}
                        {this.state.errors.cpf !== '' && (
                          <div className='error'>{this.state.errors.cpf}</div>
                        )}
                      </div>
                    </div>

                    <div className='form-group'>
                      <label className='col-sm-2 col-sm-2 control-label'>Telefone</label>
                      <div className='col-sm-10'>
                        <InputMask mask='(99) 99999 99-99'
                          name='telefone'
                          value={this.state.telefone}
                          onChange={this.handleChange}
                          placeholder='Telefone'
                          className={formErrors.telefone.length > 0 ? 'form-control error' : 'form-control'}
                        />
                        {formErrors.name.length > 0 && (
                          <span className='error'>{formErrors.telefone}</span>
                        )}
                        {this.state.errors.name !== '' && (
                          <div className='error'>{this.state.errors.telefone}</div>
                        )}
                      </div>
                    </div>

                    <div className='form-group'>
                      <label className='col-lg-2 col-sm-2 control-label'>Email</label>
                      <div className='col-sm-8 col-lg-8'>
                        <input
                          className={formErrors.email.length > 0 ? 'form-control error' : 'form-control'}
                          placeholder='Email'
                          type='email'
                          name='email'
                          required
                          noValidate
                          value={this.state.emailObject.email}
                          onChange={this.handleChange}
                        />
                        {formErrors.email.length > 0 && (
                          <span className='error '>{formErrors.email}</span>
                        )}
                        {this.state.errors.email !== '' && (
                          <div className='error'>{this.state.errors.email}</div>
                        )}
                      </div>
                        <div className='col-sm-2 col-lg-2'>
                            <button className='btn btn-theme' onClick={this.handleAddEmail}>Add Email</button>
                        </div>

                          {this.state.emails.map((data , index) => {
                          return <div>
                            <div className='col-sm-3 col-lg-3'>
                            <li key={index}>
                                <p>
                                  Email: {data.email} <br/>
                                </p> 
                                <button className='btn btn-danger btn-xs' 
                                   onClick={this.handleDelEmail.bind(this, data)}>Remover</button>
                             </li>
                             </div>
                          </div>
                          
                        })} 
                        <br/>
                    </div>

                    <ViaCep cep={this.state.cep} onSuccess={this.handleSuccess} lazy>
                      {({ data, loading, error, fetch }) => {
                        if (loading) {
                          return <p>loading...</p>
                        }
                        // if (error) {
                        //   return <p>Nenhum registro encontrado</p>
                        // }
                        // if (data) {
                        //   return 
                        // }
                        return <div className='form-group'>
                          <label className='col-lg-2 col-sm-2 control-label'>CEP</label>
                          <div className='col-sm-8 col-lg-8'>
                            <InputMask mask='99999-999'
                              className={'form-control'} name='cep' value={this.state.cep}
                              placeholder='CEP' onChange={this.handleChangeCep}
                            />
                            {formErrors.cep.length > 0 && (
                              <span className='error'>{formErrors.cep}</span>
                            )}
                            {this.state.errors.cep !== '' && (
                              <div className='error'>{this.state.errors.cep}</div>
                            )}
                          </div>
                          <div className='col-sm-2 col-lg-2'>
                            <button className='btn btn-theme' onClick={fetch}>Pesquisar</button>
                          </div>

                        </div>
                      }}
                    </ViaCep>

                    {this.state.endereco.cep !== '' && this.state.endereco.cep !== undefined &&(
                      <div className='form-group'>
                      <label className='col-lg-2 col-sm-2 control-label'>Endereço</label>
                      <div className='col-lg-8'>
                        <div className='col-sm-4 col-md-4'>
                          <label for='idCep' className='control-label'>CEP</label>
                          <input type='text' disabled name='cep' value={this.state.endereco.cep}
                            className='form-control' id='idCep' />
                        </div>

                        <div className='col-sm-4 col-md-4'>
                          <label for='idCidade' className='control-label'>Cidade</label>
                          <input type='text' name='endereco.localidade' disabled 
                          value={this.state.endereco.localidade} 
                            className='form-control' id='idCidade' />
                        </div>

                        <div className='col-sm-4 col-md-4'>
                          <label for='idBairro' className='control-label'>Bairro</label>
                          <input type='text' name='bairro' value={this.state.endereco.bairro}
                            onChange={this.handleChangeEndereco}
                            className='form-control' id='idBairro' />
                        </div>

                        <div className='col-sm-4 col-md-4'>
                          <label for='idLogradouro' className='control-label'>Logradouro</label>
                          <input type='text' name='logradouro'  value={this.state.endereco.logradouro}
                            onChange={this.handleChangeEndereco}
                            className='form-control' id='idLogradouro' />

                        </div>

                        <div className='col-sm-2 col-md-2'>
                          <label for='idUf' class='control-label'>UF</label>
                          <input type='text' name='uf' disabled value={this.state.endereco.uf}
                            className='form-control' id='idUf' />
                        </div>
                        
                        <div className='col-sm-4 col-md-4'>
                          <label for='idComplemento' class='control-label'>Complemento</label>
                          <input type='text' name='complemento' value={this.state.complemento}
                            className='form-control' onChange={this.handleChangeEndereco}  />

                        </div>
                      </div>

                      <div className='col-lg-2'>
                        <button className='btn btn-theme' onClick={this.cancelConsultaCep}>Cancelar</button>
                        <button className='btn btn-theme' onClick={this.handleAddEndereco}>Add</button>
                      </div>
                    </div>
                    )}
                    {this.state.enderecos.map((data , index) => {
                          return <div>
                            <div className='col-lg-3'>
                            <li key={index}>
                                <p>
                                  CEP: {data.cep} <br/>
                                  CIDADE: {data.localidade} <br/>
                                  Bairro: {data.bairro} <br/>
                                  Logradouro: {data.logradouro} <br/>
                                  Complemento: {data.complemento} <br/>
                                  UF: {data.uf} <br/>
                                </p> 
                                <button className='btn btn-xs btn-danger' 
                                   onClick={this.handleDelItem.bind(this, data)}>Remover</button>
                             </li>
                             </div>
                          </div>
                        })} 
                        <br/>
                    <div className='form-group'>
                      <div className='col-lg-offset-2 col-lg-10'>
                        <button className='btn btn-theme' onClick={this.handleSubmit}>Submit</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <br />
          </section>
        </section >
      </div >
    )
  }
}
export default FormCompenent;