import React, { Component } from 'react'
import axios from 'axios'
import InputMask from 'react-input-mask';
import {Link} from 'react-router-dom';

class Detalhar extends Component {

  constructor(props) {
    super(props);

    this.state = {
      id: '',
      name: '',
      cpf: '',
      telefone: '',
      cep: '',
      enderecos: [],
      emails: [],
    };
  }

  componentWillMount() {
    this.getMeetupDetails();
  }

  getMeetupDetails() {
    const meetupId = this.props.match.params.id;
    console.log("ID" + meetupId)
    if (meetupId !== undefined) {
      axios.get(`http://localhost:8080/api/cliente/` + meetupId)
        .then(response => {
          console.log(response);
          this.setState({
            id: response.data.id,
            name: response.data.name,
            telefone: response.data.telefone,
            cpf: response.data.cpf,
            emails: response.data.emails,
            enderecos: response.data.enderecos
          }, () => {
          });
        })
        .catch(err => console.log(err));
    }
  }

  render() {
    const name = this.state;
    return (
      <div>
        <section id='main-content'>
          <section className='wrapper'>
            <h3><i className='fa fa-angle-right'></i> Detalhar Cliente </h3>
            <div className='row mt'>

              <div className='col-lg-12'>
                <div className='form-panel'>
                  <h4 className='mb'><i className='fa fa-angle-right'></i>Detalhar</h4>
                  <form className='form-horizontal style-form' >
                    <div className='form-group'>
                      <label className='col-sm-2  control-label'>Nome</label>
                      <div className='col-sm-10'>
                        <input type='text'
                          name='name'
                          disabled
                          className='form-control'
                          value={this.state.name}
                        />
                      </div>
                    </div>
                    <div className='form-group'>
                      <label className='col-lg-2 col-sm-2 control-label'>CPF</label>
                      <div className='col-sm-10 col-lg-10'>
                      <InputMask mask='999.999.999-99'
                          className='form-control'
                          disabled
                          value={this.state.cpf}
                        />
                      </div>
                    </div>

                    <div className='form-group'>
                      <label className='col-sm-2 col-lg-2 col-sm-2 control-label'>Telefone</label>
                      <div className='col-sm-10 col-lg-10'>
                      <InputMask mask='(99) 99999 99-99'
                          className='form-control'
                          disabled
                          value={this.state.telefone}
                        />
                      </div>
                    </div>

                    <div className='form-group'>
                      <label className='col-lg-2 col-sm-2 control-label'>Email</label>
                      
                      {this.state.emails.map((data , index) => {
                          return <div>
                            <div className='col-sm-3 col-lg-3'>
                            <li key={index}>
                                <p>
                                  Email: {data.email} <br/>
                                </p> 
                             </li>
                             </div>
                          </div>
                          
                        })} 
                      <br />
                    </div>

                    <div className='form-group'>
                      <label className='col-lg-2 col-sm-2 control-label'>Endereco</label>
                      {this.state.enderecos.map((data, index) => {
                        return <div>
                          <div className='col-sm-3 col-lg-3'>
                            <li key={index}>
                              <p>
                                CEP: {data.cep} <br />
                                CIDADE: {data.localidade} <br />
                                Bairro: {data.bairro} <br />
                                Logradouro: {data.logradouro} <br />
                                Complemento: {data.complemento} <br />
                                UF: {data.uf} <br />
                              </p>
                            </li>
                          </div>
                        </div>
                      })}
                      <br />
                    </div>


                    <div className='form-group'>
                      <div className='col-lg-offset-2 col-lg-10'>
                      <Link className='btn btn-theme'  to={`/lista/`} >Voltar</Link>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
          </section>
        </section >
      </div >
    )
  }
}
export default Detalhar;