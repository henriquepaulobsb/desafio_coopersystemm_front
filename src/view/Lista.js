import React, { Component } from 'react';
import Pagination from "react-js-pagination";
import {Link} from 'react-router-dom';

class Lista extends Component {
    constructor(props) {
        super(props);
        this.state = {
            items: [],
            totalElements: 0,
            totalPages:0,
            pageNumber:0,
            offset:0,
            number:1,
            itemsPorPage: 0,
            activePage: 0,
            first: false,
            last: false
        }
        this.fetchData= this.fetchData.bind(this);
    }

    componentDidMount() {
        this.fetchData();
    }

    edit = (item) => {

    }

    fetchData(pageNumber) {
        pageNumber = pageNumber -1;
        this.setState({
            items: []
        })
            fetch('http://localhost:8080/api/clientePaginada?page='+pageNumber +'&size=10')
            .then(response => response.json())
            .then(json => this.setState(
                {
                    items: json.content,
                    totalElements: json.totalElements,
                    pageNumber: json.pageable.pageNumber,
                    offset: json.pageable.offset,
                    totalPages: json.totalPages,
                    itemsPorPage: json.numberOfElements,
                    number: json.number,
                    first: json.first,
                    last: json.last,
                    activePage: pageNumber +1,
                }
            ))
            .catch(error => console.log('parsing failed Falhou', error))
    }

    render() {
        console.log(this.state)
        const {items } = this.state;
        return (
            <div>
                <section id='main-content'>
                    <section className='wrapper'>
                        <h3><i className='fa fa-angle-right'></i> Data Table</h3>
                        <div className='row'>
                            <div className='col-md-12'>
                                <div className='content-panel'>
                                    <div style={{textAlign: 'center',display: 'flow-root'}}>
                                    <Pagination style={{ display: 'flowroot'}}
                                       activePage={this.state.pageNumber +1}
                                        itemsCountPerPage={this.state.numberOfElements}
                                        totalItemsCount={this.state.totalElements}
                                        pageRangeDisplayed={5}
                                        onChange={this.fetchData}
                                    />

                                   <div className="results" wp-id="687">
                                       Página <strong wp-id="688">{this.state.pageNumber +1} </strong> de |
                                       <strong wp-id="690">{this.state.totalPages}</strong>     |                                    
                                        do <strong wp-id="690"></strong>total de | 
                                        <strong wp-id="689">{this.state.offset + this.state.itemsPorPage}</strong>registro
                                        <strong wp-id="689">{this.state.totalElements}</strong> 
                                   </div>

                                     </div>
                                    <table className='table table-striped table-advance table-hover' >
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nome</th>
                                                <th>Email</th>
                                                <th>CPF</th>
                                                <th ></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                         {
                                            items.map(item => (
                                                <tr key={item.id}>
                                                    <td>{item.id}</td>
                                                    <td>{item.name}</td>
                                                    <td>{item.email}</td>
                                                    <td>{item.cpf}</td>
                                                    <td>
                                                    <div className="pull-right hidden-phone">
                                                        <Link className="btn btn-primary btn-xs fa fa-pencil" style={{margin: '3px'}} to={`/update/${item.id}`}></Link>
                                                        <Link className="btn btn-success btn-xs fa fa fa-tasks" to={`/detalhar/${item.id}`}></Link>
                                                        </div>
                                                    </td>

                                                    
                                                </tr>
                                            ))
                                            }
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </section>
                </section>
            </div>
        )
    }
}
export default Lista;
